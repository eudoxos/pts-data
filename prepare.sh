#!/bin/bash
set -e -x
mkdir -p dist
mkdir -p src/datasources/scbil/data/data
mkdir -p src/datasources/scbil/data/cache

function download_sc () {
  echo "Downloading SC data"
  wget -q https://github.com/suttacentral/sc-data/archive/refs/heads/master.zip -O master.zip
  unzip -qq master.zip
  rm -rf src/datasources/scbil/data/data/sc-data-master
  mv sc-data-master src/datasources/scbil/data/data/
}

function download_bilara () {
  echo "Downloading Bilara data"
  wget -q https://github.com/suttacentral/bilara-data/archive/refs/heads/published.zip -O published.zip
  unzip -qq published.zip
  rm -rf src/datasources/scbil/data/data/bilara-data-published
  mv bilara-data-published src/datasources/scbil/data/data/
}

function clear_cache () {
  find src/datasources/scbil/data/cache/ -name "*.json" -exec rm {} \;
}

CODE_VERSION_FILE="src/datasources/scbil/data/data/version-sc-bil-code.txt"
CODE_VERSION=$(cat $CODE_VERSION_FILE || echo "NONE")
CODE_VERSION_NEW=$(find src/datasources/scbil/ -name "*.py" -not -path "src/datasources/scbil/test/*" -exec sha256sum {} \; | sha256sum | cut -d' ' -f1)
if [[ $CODE_VERSION != $CODE_VERSION_NEW ]]; then
  echo "Code changed"
  clear_cache
  echo $CODE_VERSION_NEW > $CODE_VERSION_FILE
else
  echo "No code changed"
fi

SC_VERSION_FILE="src/datasources/scbil/data/data/version-sc-data.txt"
SC_VERSION=$(cat $SC_VERSION_FILE || echo "NONE")
SC_VERSION_NEW=$(git ls-remote https://github.com/suttacentral/sc-data.git refs/heads/master | cut -f1)
if [[ $SC_VERSION != $SC_VERSION_NEW ]]; then
  download_sc
  clear_cache
  echo $SC_VERSION_NEW > $SC_VERSION_FILE
elif [[ ! -z $CI ]]; then
  echo "No new SC version"
  download_sc
else
  echo "No new SC version"
fi

BILARA_VERSION_FILE="src/datasources/scbil/data/data/version-bilara-data.txt"
BILARA_VERSION=$(cat $BILARA_VERSION_FILE || echo "NONE")
BILARA_VERSION_NEW=$(git ls-remote https://github.com/suttacentral/bilara-data.git refs/heads/published | cut -f1)
if [[ $BILARA_VERSION != $BILARA_VERSION_NEW ]]; then
  download_bilara
  clear_cache
  echo $BILARA_VERSION_NEW > $BILARA_VERSION_FILE
elif [[ ! -z $CI ]]; then
  echo "No new Bilara version"
  download_bilara
else
  echo "No new Bilara version"
fi

ls -l src/datasources/scbil/data/cache/
