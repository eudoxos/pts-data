from glob import glob
from bs4 import BeautifulSoup
from os import path
import re

def parse_pts_ref(s: str):
	""" Try to parse volume, page of a reference. Return None if not possible. """

	match = re.match(r'^\{([1-9])\.([0-9]{1,3})\}$', s)

	if match == None:
		return None

	volume = int(match.group(1))
	page = int(match.group(2))

	return volume, page

def get_reference_anchor_link(html_file, pts_span, soup):
	""" Create a link to position of the given reference. """

	return path.join(
		'https://www.ancient-buddhist-texts.net/English-Texts/Buddhist-Legends/',
		path.basename(html_file) + '#:~:text=' + str(pts_span.text)
	)

def extract_references(html_dir: str):
	""" Extract references / links for dhpa. """

	pts_references = []

	html_files = sorted(glob(path.join(html_dir, '*.htm')))
	for html_file in html_files:
		with open(html_file) as f:
			soup = BeautifulSoup(f.read(), 'html.parser')

		for span in soup.find_all('span', class_='number'):
			parsed = parse_pts_ref(span.text)

			if not parsed:
				continue

			volume, page = parsed
			excerpt = str(span.next_sibling)
			excerpt = excerpt.strip()[:300] if excerpt else ''

			pts_references.append({
				'datasource': 'ancient-buddhist-texts.net',
				'edition': 0,
				'book': 'Dhp-A',
				'volume': volume,
				'page': page,
				'links': [
					{
						'language': 'en',
						'authorName': 'Eugene Watson Burlingame',
						'url': get_reference_anchor_link(html_file, span, soup),
						'excerpt': excerpt
					}
				]
			})

	return pts_references