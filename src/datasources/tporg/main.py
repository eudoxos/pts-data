import json
from os import path
from parser.parse_toc import parse_toc, find_book_from_hierarchy
from parser.extract_references import extract_references

if __name__ == '__main__':
  DIST_DIR = path.join(path.dirname(__file__), '../../../dist/')
  TIPITIKA_DIR = path.join(path.dirname(path.realpath(__file__)), 'data/xmlfiles/')

  toc_tree, text_files = parse_toc()

  text_data = []
  for (text_name, text_file), hier in text_files:

    # TODO: figure out book names here
    if hier[0] == 'Pañcapakaraṇa-aṭṭhakathā':
      continue

    book_name, book_abbrev = find_book_from_hierarchy(hier)
    pts_refs = extract_references(path.join(TIPITIKA_DIR, text_file))

    if book_abbrev == 'Nidd-I':
      # set volume to 0 in this case to match style of SC references
      # Also, volume 2 does not start with 1, which is weird.
      for ref in pts_refs:
        ref['volume'] = 0

    text_data.append({
      'textName': text_name,
      'textFile': text_file,
      'book': book_abbrev,
      'bookName': book_name,
      'hierarchy': hier,
      'pts_refs': pts_refs
    })

  # restructure to fit correct json format
  pts_data = []
  for item in text_data:
    for ref in item['pts_refs']:
      url = 'https://tipitaka.org/romn/' + item['textFile']

      if 'paragraph' in ref and ref['paragraph']:
        url += '#para' + ref['paragraph']

      text_name = ''

      if not ref['chapter']:
        text_name = ref['subhead']
      elif not ref['subhead']:
        text_name = ref['chapter']
      else:
        text_name = '%s / %s' % (ref['chapter'], ref['subhead'])

      pts_data.append({
        'datasource': 'tipitika.org',
        'edition': 0,
        'book': item['book'],
        'volume': ref['volume'],
        'page': ref['page'],
        'textName': text_name,
        'hierarchy': item['hierarchy'],
        'links': [{
          'language': 'pli',
          'url': url,
          'excerpt': ref['excerpt']
        }]
      })


  with open(path.join(DIST_DIR, 'tporg_text.json'), 'w') as f:
    f.write(json.dumps(text_data, indent=2))

  with open(path.join(DIST_DIR, 'tporg.json'), 'w') as f:
    f.write(json.dumps(pts_data, indent=2))